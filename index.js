const express = require("express");
const app = express();
const router = express.Router();
const config = require('./config');

const reqestID = require('express-request-id');

const errorHandling = require('./errorHandling');
const fs = require('fs');

const match1 = require('./src/server/1-matches-per-year.cjs');
const match2 = require('./src/server/2-matches-won-per-team-per-year.cjs')
const match3 = require('./src/server/3-extra-run-per-team.cjs');
const match4 = require('./src/server/4-top-10-economic-blower-year-2015.cjs');
const match5 = require('./src/server/5-team-won-toss-won-match.cjs');
const match6 = require('./src/server/6-highest-player-of-match-per-year.cjs');
const match7 = require('./src/server/7-batsman-strike-rate-per-year.cjs');
const match8 = require('./src/server/8-highest-no-one-bowler-take-dissmail.cjs');
const match9 = require('./src/server/9-bowler-best-economy-super-over.cjs');

app.use(reqestID());

app.use((req, res, next) => {
    fs.appendFile('url.log', req.url + " " + req.id + '\n', (err) => {
        if (err) {
            console.log(err);
        }

    })
    next();
})

app.use(router);

router.get('/', (req, res) => {
    
    res.send(`<!DOCTYPE html><html>
    <head></head>
    <body style="text-align:center">
    
        <h1> Welcome </h1>
        <h1> Below the url link of endpoint </h1>
    
        <h2><a  href='/1-matches-per-year'/>matches-per-year</h2>
        <h2><a  href='/2-matches-won-per-team-per-year'/>matches-won-per-team-per-year</h2>
        <h2><a  href='/3-extra-run-per-team'/>3-extra-run-per-team</h2>
        <h2><a  href='/5-team-won-toss-won-match'/>team-won-toss-won-match</h2>
        <h2><a  href='/6-highest-player-of-match-per-year'/>6-highest-player-of-match-per-year</h2>
        <h2><a  href='/7-batsman-strike-rate-per-year'/>7-batsman-strike-rate-per-year</h2>
        <h2><a  href='/8-highest-no-one-bowler-take-dissmail'/>8-highest-no-one-bowler-take-dissmail</h2>
        <h2><a  href='/9-bowler-best-economy-super-over'/>9-bowler-best-economy-super-over</h2>



    
    
    </body>
    </html>`).end();
});

router.get('/1-matches-per-year', match1);
router.get('/2-matches-won-per-team-per-year', match2);
router.get('/3-extra-run-per-team', match3);
router.get('/4-top-10-economic-blower-year-2015', match4);
router.get('/5-team-won-toss-won-match', match5);
router.get('/6-highest-player-of-match-per-year', match6);
router.get('/7-batsman-strike-rate-per-year', match7);
router.get('/8-highest-no-one-bowler-take-dissmail', match8);
router.get('/9-bowler-best-economy-super-over', match9);

router.get('/logs', (req, res) => {
    fs.readFile('url.log', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(data);
        }
    })
});




app.use(errorHandling);



app.use((req, res, next) => {
   res.status(404).json({ error : "Invalid url" })
});



app.listen(config.port, () => {
    console.log("server is running");
})