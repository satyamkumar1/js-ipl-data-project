
const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';



const matchWonPerTeamPerYear = (req, res, next) => {

    csvToJson()
        .fromFile(matchData)
        .then((data) => {

            const ans = data.reduce((acc, item) => {

                if (!acc[item.season]) {
                    acc[item.season] = {};
                }
                if (!acc[item.season][item.winner]) {
                    acc[item.season][item.winner] = 0;
                }
                acc[item.season][item.winner] += 1;

                return acc;

                // let flag = false;

                // let arr = acc.find((ipldata) => {
                //     return (ipldata.season == item.season && ipldata.team == item.winner);

                // })


                // if (arr) {
                //     flag = true;
                //     console.log(arr);
                //     arr.won += 1;
                // }
                // if (!flag) {
                //     const obj = {
                //         "season": item.season,
                //         "team": item.winner,
                //         "won": 1
                //     }
                //     acc.push(obj);
                // }

                // return acc;

            }, {});
            res.send(ans);

        })
        .catch((err) => {
            // console.log(err);
            next({
                err: err.message
            })
        })

}

module.exports = matchWonPerTeamPerYear;