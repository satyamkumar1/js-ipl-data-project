
const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';
const deliveriesData = 'src/data/deliveries.csv';


const  economicBowler=(req,res,next)=> {

        csvToJson()
                .fromFile(matchData)
                .then((data) => {

                        let arr = data.reduce((acc, item) => {

                                if (item.season == 2015) {
                                        acc.push(item.id);
                                }
                                return acc;

                        }, []);

                        // console.log(arr);

                        csvToJson()
                                .fromFile(deliveriesData)
                                .then((data) => {


                                        const result = data.reduce((acc, item) => {

                                                if (arr.includes(item.match_id)) {

                                                        if (acc[item.bowler] === undefined) {
                                                                acc[item.bowler] =
                                                                {
                                                                        run: Number(item.total_runs),
                                                                        ball: 1,
                                                                        economy: 6
                                                                }


                                                        }
                                                        else {

                                                                acc[item.bowler].run += Number(item.total_runs);
                                                                acc[item.bowler].ball += 1;
                                                                acc[item.bowler].economy = (acc[item.bowler].run * 6) / acc[item.bowler].ball;
                                                        }

                                                }


                                                return acc;

                                        }, {});
                                        const keys = Object.keys(result);

                                        keys.sort((bowler1, bowler2) => {
                                                // console.log("b" + bowler1)
                                                if (res[bowler1].economy > res[bowler2].economy) {
                                                        return 1;
                                                }
                                                if (res[bowler1].economy < res[bowler2].economy) {
                                                        return -1;
                                                }
                                                else {
                                                        return 0;
                                                }

                                        })

                                        res.send(keys.slice(0, 10));                                                                 


                                       
                                }) .catch((err) => {
                                        // console.log(err);
                                        next({
                                            err: err.message
                                        })
                                    })
                                
                });

}
module.exports=economicBowler;