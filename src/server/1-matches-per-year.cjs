const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';



const matchPerYearData = (req, res, next) => {

    csvToJson()
        .fromFile(matchData)
        .then((data) => {

            const totalMatchperYear = data.reduce((acc, items) => {
                //console.log(items[0]);
                if (!acc[items.season]) {
                    acc[items.season] = 1
                }
                else {
                    acc[items.season] += 1;
                }
                return acc;
            }, {});

            res.json(totalMatchperYear);

        })
        .catch((err) => {
            // console.log(err);
            next({
                err : err.message
            })
        })
}

module.exports = matchPerYearData;

