const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';
const deliveriesData = 'src/data/deliveries.csv';


const highestDissmialsByBowler=(req,res,next)=> {

    csvToJson()
        .fromFile(deliveriesData)
        .then((data) => {


            const result = data.reduce((acc, item) => {

                if (item.player_dismissed != '') {
                    let flag = false;

                    let getPersentData = acc.find((ipldata) => {
                        return (ipldata.battingplayer == item.player_dismissed && ipldata.bowlingplayer == item.bowler);


                    });


                    if (getPersentData) {
                        flag = true;
                        getPersentData.out += 1;
                    }
                    if (!flag) {
                        const obj = {
                            "battingplayer": item.player_dismissed,
                            "bowlingplayer": item.bowler,
                            "out": 1
                        }
                        acc.push(obj);
                    }
                }
                return acc;

            }, []);
            result.sort((bowler1, bowler2) => {
                if (bowler1.out > bowler2.out) {
                    return -1;
                }
                if (bowler1.out < bowler2.out) {
                    return 1;
                }
                else {
                    return 0;
                }
            })

            res.send(result[0]);
                      
        })
        .catch((err) => {
            // console.log(err);
            next({
                err: err.message
            })
        })

}

module.exports= highestDissmialsByBowler;