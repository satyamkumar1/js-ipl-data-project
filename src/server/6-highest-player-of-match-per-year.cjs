const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';



const playerOfTheMatch = (req, res, next) => {
    csvToJson()
        .fromFile(matchData)
        .then((data) => {
            //console.log(data);
            const players = data.reduce((acc, item) => {
                let boolean = false;

                const arr = acc.filter((accIdx) => {
                    return (
                        item.player_of_match == accIdx.name &&
                        item.season == accIdx.year
                    );
                });
                if (arr.length) {
                    boolean = true;
                    arr[0].potm += 1;
                }

                if (!boolean) {
                    let matchWonObj = {
                        year: item.season,
                        name: item.player_of_match,
                        potm: 1,
                    };
                    acc.push(matchWonObj);
                }

                return acc;
            }, []);
            players.sort(compare);


            const playerOfTheYear = players.reduce((acc, data) => {
                if (acc[data.year] === undefined) {
                    acc[data.year] = {};
                    acc[data.year][data.name] = data.potm;
                }
                return acc;
            }, {});

            res.send(playerOfTheYear);
            
        })
        .catch((err) => {
            // console.log(err);
            next({
                err: err.message
            })
        })

    function compare(a, b) {
        if (parseInt(a.year) < parseInt(b.year)) {
            return 1;
        } else if (parseInt(a.year) > parseInt(b.year)) {
            return -1;
        } else if (parseInt(a.potm) < parseInt(b.potm)) {
            return 1;
        } else if (parseInt(a.potm) > parseInt(b.potm)) {
            return -1;

        }
    }
}


module.exports = playerOfTheMatch;