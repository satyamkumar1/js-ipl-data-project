const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';
const deliveriesData = 'src/data/deliveries.csv';


const getJsonData=(req,res,next)=> {
    
    csvToJson()
        .fromFile(matchData)
        .then((data) => {

            const arr = data.reduce((acc, item) => {
                acc.push(item.season);
                return acc;
            }, []);


            csvToJson()
                .fromFile(deliveriesData)
                .then((data) => {

                    let ans = data.reduce((acc, item) => {

                        let flag = false;

                        const year = arr[item.match_id];

                        let getData = acc.find((ipldata) => {
                            return (ipldata.batsman1 === item.batsman);

                        })

                        if (getData) {
                            flag = true;
                           
                            getData.totalRun += Number(item.batsman_runs);
                            getData.bowl += 1;
                            getData.strikRate = (getData.totalRun * 100) / getData.bowl;

                        }

                        if (!flag) {
                            let initalStrick = (item.batsman_runs * 100) / 1;

                            let obj = {
                                per_year: year,
                                batsman1: item.batsman,
                                totalRun: Number(item.batsman_runs),
                                bowl: 1,
                                strikRate: initalStrick

                            }
                            acc.push(obj);
                        }

                        return acc;

                    }, []);
                    res.send(ans);
                    

                })
                .catch((err) => {
                    // console.log(err);
                    next({
                        err: err.message
                    })
                })
        })

}
module.exports= getJsonData;