const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';



const teamWonTossWonMatch = (req, res, next) => {

    csvToJson()
        .fromFile(matchData)
        .then((data) => {

            const result = data.reduce((acc, item) => {

                if (item.toss_winner === item.winner) {

                    if (acc[item.winner] === undefined) {
                        acc[item.winner] = 1;
                    }
                    else {
                        acc[item.winner] += 1;
                    }
                }
                return acc;


            }, {});
            console.log(result);

            res.send(result);
            
        })
        .catch((err) => {
            // console.log(err);
            next({
                err: err.message
            })
        })
}
module.exports = teamWonTossWonMatch;