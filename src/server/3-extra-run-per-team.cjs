
const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';
const deliveriesData = 'src/data/deliveries.csv';


const extraRunPerTeam = (req, res, next) => {

    csvToJson()
        .fromFile(matchData)
        .then((data) => {

            let arrayOfmatch = data.reduce((acc, item) => {

                if (item.season == 2016) {
                    acc.push(item.id);
                }
                return acc;

            }, [])

            // console.log(arrayOfmatch);

            csvToJson()
                .fromFile(deliveriesData)
                .then((data) => {

                    const ans = data.reduce((acc, item) => {

                        if (arrayOfmatch.includes(item.match_id)) {

                            // console.log(item);
                            if (acc[item.bowling_team] === undefined) {
                                // acc[item.bowling_team] = {};
                                acc[item.bowling_team] = Number(item.extra_runs);

                            }
                            else {

                                acc[item.bowling_team] += Number(item.extra_runs);
                            };
                        }

                        return acc;

                    }, {});

                    res.send(ans);
                    

                })
                .catch((err) => {
                    // console.log(err);
                    next({
                        err: err.message
                    })
                })
        })

}
module.exports = extraRunPerTeam;