const csvToJson = require('csvtojson');
const matchData = 'src/data/matches.csv';
const deliveriesData = 'src/data/deliveries.csv';


const superover = (req, res, next) => {

    csvToJson()
        .fromFile(deliveriesData)
        .then((data) => {


            //console.log(data);
            const superOverBowler = data.reduce((superOverBowler, items) => {
                let flag = false;
                if (items.is_super_over != 0) {

                    const bowl = superOverBowler.find((bowl) => {

                        return (bowl.bowlerName == items.bowler);

                    })

                    if (bowl) {
                        bowl.totalRun += Number(items.is_super_over);
                        bowl.totalBowl += 1;
                        bowl.economy_superOver = (bowl.totalRun * 6) / bowl.totalBowl;
                        flag = true;

                    }

                    if (!flag) {
                        let initalEconomic = (items.is_super_over * 6) / 1;
                        let obj = {
                            bowlerName: items.bowler,
                            totalRun: 1,
                            totalBowl: 1,
                            economy_superOver: initalEconomic

                        }
                        superOverBowler.push(obj);

                    }


                }
                return superOverBowler;


            }, []);
            //console.log(superOverBowler);

            superOverBowler.sort((bowler1, bowler2) => {
                if (bowler1.economy_superOver > bowler2.economy_superOver) {
                    return 1;
                }
                if (bowler1.economy_superOver < bowler2.economy_superOver) {
                    return -1;
                }
                else {
                    return 0;
                }
            })

            res.send(superOverBowler[0]);
            
        })
        .catch((err) => {
            // console.log(err);
            next({
                err: err.message
            })
        })



}
module.exports = superover;