const errorHandling = (err, req, res, next) => {

    // console.log(err.message);
    res.status(400);
    res.json({ "message": err.message});

}
module.exports = errorHandling;